-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 23 nov. 2021 à 07:59
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `games`
--

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20211103092843', '2021-11-03 09:30:08', 323),
('DoctrineMigrations\\Version20211103131637', '2021-11-03 13:16:41', 186),
('DoctrineMigrations\\Version20211103132152', '2021-11-03 13:22:05', 188),
('DoctrineMigrations\\Version20211103132750', '2021-11-03 13:27:54', 119),
('DoctrineMigrations\\Version20211103132945', '2021-11-03 13:29:50', 176),
('DoctrineMigrations\\Version20211103133129', '2021-11-03 13:31:33', 102),
('DoctrineMigrations\\Version20211103133309', '2021-11-03 13:33:12', 202),
('DoctrineMigrations\\Version20211103151828', '2021-11-03 15:18:44', 637),
('DoctrineMigrations\\Version20211104135846', '2021-11-04 13:58:59', 459),
('DoctrineMigrations\\Version20211109093511', '2021-11-09 09:36:35', 416),
('DoctrineMigrations\\Version20211109143050', '2021-11-09 14:30:57', 160),
('DoctrineMigrations\\Version20211110113226', '2021-11-10 11:32:30', 482),
('DoctrineMigrations\\Version20211116092228', '2021-11-16 10:12:25', 202),
('DoctrineMigrations\\Version20211121113045', '2021-11-21 11:32:01', 378);

-- --------------------------------------------------------

--
-- Structure de la table `games`
--

DROP TABLE IF EXISTS `games`;
CREATE TABLE IF NOT EXISTS `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favorite` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `games`
--

INSERT INTO `games` (`id`, `title`, `description`, `image`, `date`, `url`, `favorite`) VALUES
(1, 'Alldarso', 'Le Casse brique de l\'espace !', 'alldarso.jpg', '2021-03-25 00:00:00', 'https://lesdieuxdudev.lecoledunumerique.fr/jeux/alldarso/', 1),
(2, 'alpha-path', 'Survivre le plus longtemps possible !', 'alpha-path.jpg', '2021-09-17 00:00:00', 'https://lesdieuxdudev.lecoledunumerique.fr/jeux/alpha-path/', 0),
(3, 'escape-ball', 'Tu dois t\'échapper that sure !', 'escape-ball.jpg', '2020-03-24 00:00:00', 'https://lesdieuxdudev.lecoledunumerique.fr/jeux/escape-ball/', 0),
(4, 'ez-game', 'Qui arriveras a finir le plus vite possible ?', 'ez-game.jpg', '2021-09-20 00:00:00', 'https://lesdieuxdudev.lecoledunumerique.fr/jeux/ez-game/', 0),
(5, 'Flag_Conqueror', 'Vien on ce tape !', 'flag_conqueror.jpg', '2021-03-24 00:00:00', 'https://lesdieuxdudev.lecoledunumerique.fr/jeux/Flag_Conqueror/', 1),
(6, 'lheure_du_gouter', 'J\'ai faim !', 'heure-du-gouter.jpg', '2021-03-24 00:00:00', 'https://lesdieuxdudev.lecoledunumerique.fr/jeux/lheure_du_gouter/', 0),
(7, 'picnic-break', 'le casse brique de la hess', 'picnic-break.jpg', '2021-09-21 00:00:00', 'https://lesdieuxdudev.lecoledunumerique.fr/jeux/picnic-break/', 0),
(10, 'siren-trash', 'soit écolo mon ami  !', 'siren-trash.jpg', '2021-09-17 00:00:00', 'https://lesdieuxdudev.lecoledunumerique.fr/jeux/siren-trash/', 1);

-- --------------------------------------------------------

--
-- Structure de la table `games_genres`
--

DROP TABLE IF EXISTS `games_genres`;
CREATE TABLE IF NOT EXISTS `games_genres` (
  `games_id` int(11) NOT NULL,
  `genres_id` int(11) NOT NULL,
  PRIMARY KEY (`games_id`,`genres_id`),
  KEY `IDX_6AC30AA397FFC673` (`games_id`),
  KEY `IDX_6AC30AA36A3B2603` (`genres_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `games_genres`
--

INSERT INTO `games_genres` (`games_id`, `genres_id`) VALUES
(1, 1),
(2, 7),
(2, 8),
(3, 5),
(4, 5),
(5, 9),
(6, 6),
(7, 1),
(10, 5);

-- --------------------------------------------------------

--
-- Structure de la table `games_users`
--

DROP TABLE IF EXISTS `games_users`;
CREATE TABLE IF NOT EXISTS `games_users` (
  `games_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`games_id`,`users_id`),
  KEY `IDX_EB93B75597FFC673` (`games_id`),
  KEY `IDX_EB93B75567B3B43D` (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `games_users`
--

INSERT INTO `games_users` (`games_id`, `users_id`) VALUES
(1, 8),
(1, 9),
(2, 8),
(5, 8),
(10, 9);

-- --------------------------------------------------------

--
-- Structure de la table `genres`
--

DROP TABLE IF EXISTS `genres`;
CREATE TABLE IF NOT EXISTS `genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `genres`
--

INSERT INTO `genres` (`id`, `title`, `description`, `image`) VALUES
(1, 'casse brique', 'Le casse-briques est un genre de jeu vidéo souvent classé dans la catégorie arcade, apparu en 1975', 'cassebrique.jpg'),
(2, 'bataille navale', 'La bataille navale, appelée aussi touché-coulé, est un jeu de société dans lequel deux joueurs doivent placer des « navires » sur une grille tenue secrète et tenter de « toucher » les navires adverses.', 'bataillenavale.jpg'),
(3, 'jeu de combat 1vs1', 'il faudra être prêt pour la baston et la castagne, avec vos poings,', 'combat1V1.jpg'),
(4, 'Tetris', 'Tetris est un jeu vidéo de puzzle', 'tetris.jpg'),
(5, 'plateforme', 'Un jeu de plates-formes ou jeu de plateformes est un genre de jeu vidéo, sous-genre du jeu d\'action. Dans les jeux de plates-formes, le joueur contrôle un avatar qui doit sauter sur des plates-formes suspendues dans les airs et éviter des obstacles.', 'plateforme.jpg'),
(6, 'Snake', 'eu vidéo dans lequel le joueur dirige un serpent qui grandit et constitue ainsi lui-même un obstacle.', 'snake.jpg'),
(7, 'Action', 'Le jeu d\'action est un genre de jeu vidéo dont le gameplay est fondé sur des interactions en temps réel et qui fait essentiellement appel à l\'habileté et aux réflexes du joueur.', 'action.jpg'),
(8, 'Aventure', 'la narration plutôt que sur les réflexes et l\'action. Plus précisément, les jeux d\'aventure mettent le plus souvent l\'accent sur l\'exploration, les dialogues, la résolution d\'énigmes.', 'aventure.jpg'),
(9, '2 VS 2', 'il faudra être prêt pour la baston et la castagne, avec vos poings en équipes', '2v2.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `note` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_11BA68CA76ED395` (`user_id`),
  KEY `IDX_11BA68CE48FD905` (`game_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `notes`
--

INSERT INTO `notes` (`id`, `date`, `etat`, `description`, `user_id`, `game_id`, `note`) VALUES
(4, '2021-11-21 11:36:53', 1, 'j\'adore pas', 8, 6, 2),
(5, '2021-11-22 10:05:02', 1, 'lourd', 8, 1, 4),
(7, '2021-11-22 11:03:45', 1, 'Trop lourd', 8, 1, 5),
(8, '2021-11-22 13:00:03', 1, 'C nul de fou', 8, 1, 1),
(9, '2021-11-23 07:45:35', 1, 'trop bien', 8, 2, 3),
(10, '2021-11-23 07:45:44', 1, 'trop bien !!!', 8, 2, 5);

-- --------------------------------------------------------

--
-- Structure de la table `reset_password_request`
--

DROP TABLE IF EXISTS `reset_password_request`;
CREATE TABLE IF NOT EXISTS `reset_password_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `selector` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashed_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `expires_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_7CE748AA76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `reset_password_request`
--

INSERT INTO `reset_password_request` (`id`, `user_id`, `selector`, `hashed_token`, `requested_at`, `expires_at`) VALUES
(3, 8, 'MkS2rpevMiJZYV2XWJ4b', 'OzmWEu+VB/lt0hUeZ1Eul5fx879nCM4xldgqimFA460=', '2021-11-22 14:39:42', '2021-11-22 15:39:42');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pseudonyme` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1483A5E9E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `roles`, `password`, `email`, `pseudonyme`) VALUES
(8, '[\"ROLE_ADMIN\"]', '$2y$13$RAn84oeVbs252vjEu3kKd.PMsS56xDbFIweNHrl0ep/9SORvWnEq6', 'zeghah43@gmail.com', 'meidhi Z'),
(9, '[]', '$2y$13$0mBgkMwWIdcHBgdaKwOt3uub56fSUxPO7zfeyXu0ZkQ9pq/tTx96e', 'inscrit.11@gmail.com', 'jesuisinscrit');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `games_genres`
--
ALTER TABLE `games_genres`
  ADD CONSTRAINT `FK_6AC30AA36A3B2603` FOREIGN KEY (`genres_id`) REFERENCES `genres` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6AC30AA397FFC673` FOREIGN KEY (`games_id`) REFERENCES `games` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `games_users`
--
ALTER TABLE `games_users`
  ADD CONSTRAINT `FK_EB93B75567B3B43D` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_EB93B75597FFC673` FOREIGN KEY (`games_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `FK_11BA68CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_11BA68CE48FD905` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`);

--
-- Contraintes pour la table `reset_password_request`
--
ALTER TABLE `reset_password_request`
  ADD CONSTRAINT `FK_7CE748AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
