<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Games;
use App\Entity\Genres;
use App\Form\GenresType;
use App\Repository\GamesRepository;
use App\Repository\GenresRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class GenresController extends AbstractController
{
    #[Route('/genres', name: 'genres')]
    public function index(GenresRepository $repo): Response
    {
        $genres = $repo->findAll();

        return $this->render('genres/index.html.twig', [
            'controller_name' => 'GenresController',
            'genres' => $genres,
        ]);
    }

 /*   #[Route('/genres/new', name: 'genres_create')]
    #[Route('/genres/{id}/edit', name: 'genres_edit')]
    public function edit(Genres $genre = null, Request $request, EntityManagerInterface $manager, SluggerInterface $slugger): Response
    {

        if(!$genre) {
            $genre = new Genres();
        }

        $form = $this->createForm(GenresType::class, $genre);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            if(!$genre->getId()) {

                $gamesFile = $form->get('Image')->getData();

            // so the file must be processed only when a file is uploaded
            if ($gamesFile) {
                $originalFilename = pathinfo($gamesFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$gamesFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $gamesFile->move(
                        $this->getParameter('images_genres_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $genre->setImage($newFilename);
                }
            }

            $manager->persist($genre);
            $manager->flush();
        }

        return $this->render('genres/edit.html.twig', [
            'formGenres' => $form->createView(),
            'editMode' => $genre->getId() !== null
        ]);
    }
    */
}
