<?php

namespace App\Controller\Admin;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use App\Entity\Notes;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class NotesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Notes::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('ID'),
            Field::new('Date'),
            Field::new('Etat'),
            Field::new('Description'),
            Field::new('note'),
            
        ];
    } 

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
