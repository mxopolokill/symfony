<?php

namespace App\Controller\Admin;

use App\Entity\Users;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UsersCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Users::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('Pseudonyme'),
            Field::new('email'),
            ChoiceField::new('roles')->setChoices([
                'ROLE_ADMIN'=>'ROLE_ADMIN ', 
                'ROLE_USER'=>'ROLE_USER'
                    ])->allowMultipleChoices(),
            Field::new('password'),
            AssociationField::new('games')
                ->autocomplete()
        ];
    }
}

