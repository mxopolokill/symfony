<?php

namespace App\Controller\Admin;

use App\Entity\Games;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Vich\UploaderBundle\Form\Type\VichImageType;


class GamesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Games::class;
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('title'),

            Field::new('Description'),

            Field::new(propertyName: 'ImageFile')->setFormType(formTypeFqcn:  VichImageType::class),
            Field::new('Image'),
            Field::new('Date'),
            Field::new('Url'),
            Field::new('favorite'),
            AssociationField::new('genres')
                ->autocomplete(),
            AssociationField::new('users')
                ->autocomplete(),

              
            
        ];
    } 
    
}




    
      
    
    
    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */

