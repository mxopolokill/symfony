<?php

namespace App\Controller\Admin;

use App\Entity\Genres;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class GenresCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Genres::class;
            
    }


     
    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('title'),

            Field::new('Description'),

            Field::new(propertyName: 'Uploadfile')->setFormType(formTypeFqcn:  VichImageType::class),
            Field::new('Image'),

            AssociationField::new('games')
            ->autocomplete(),
            
           

            
        ];
    } 
    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
