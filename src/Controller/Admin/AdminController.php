<?php

namespace App\Controller\Admin;

use App\Repository\GamesRepository;
use App\Repository\GenresRepository;
use App\Repository\NotesRepository;
use App\Repository\UsersRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Games;
use App\Entity\Genres;
use App\Entity\Notes;
use App\Entity\Users;



class AdminController extends AbstractDashboardController
{
    protected $gamesRepository;
    protected $usersRepository;
    protected $notesRepository;
    protected $GenresRepository;

    public function __construct(
        GamesRepository $gamesRepository,
        UsersRepository $usersRepository,
        NotesRepository $notesRepository,
        GenresRepository $GenresRepository
    )
    {
       $this->usersRepository = $usersRepository;
       $this->gamesRepository = $gamesRepository;
       $this->notesRepository = $notesRepository;
       $this->GenresRepository = $GenresRepository;
    }



        //Ajout route admin 
    /**
     * @Route("/admin", name="admin")
     * 
     */
    public function index(): Response
    {
        //séléctioner fichier de rendu & récupération des tables et de leurs fonctions.        
        return $this->render('bundles/EasyAdminBundle/welcome.html.twig', [
            'countUser' => $this->usersRepository->countAllusers(),
            'countGenre' => $this->GenresRepository->countAllgenres(),
            'countJeux' => $this->gamesRepository->countAllgames(),
            'countNotes' => $this->notesRepository->countAllnotes()
        ]);
    }

    //Création du dashboard
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Symfony');
    }
    
    //configuration des Items de menu 
    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Users', 'fas fa-users', Users::class);
        yield MenuItem::linkToCrud('Games', 'fas fa-gamepad', Games::class);
        yield MenuItem::linkToCrud('Genres', 'far fa-list-alt', Genres::class);
        yield MenuItem::linkToCrud('Notes', 'far fa-calendar-check', Notes::class);
       
    }
}

