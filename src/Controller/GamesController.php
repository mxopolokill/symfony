<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Notes;
use App\Entity\Users;
use App\Form\GamesType;
use App\Form\NotesType;
use App\Repository\GamesRepository;
use App\Repository\GenresRepository;
use App\Repository\NotesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Security\Core\User\User;

class GamesController extends AbstractController
{

    #[Route('/games', name: 'games')]
    public function index(GamesRepository $repo, GenresRepository $repogenres): Response
    {
        if (isset($_GET['genres'])) {
            $id_genres = $_GET['genres'];
            $games_genres = $repo->findByGenres($id_genres);
        } elseif (isset($_GET['order'])) {
            if ($_GET['order'] == 'asc') {
                $games = $repo->findBy(array(), array('title' => 'ASC'));
            }
            if ($_GET['order'] == 'desc') {
                $games = $repo->findBy(array(), array('title' => 'DESC'));
            }
        } else {
            if (isset($_GET['fav'])) {
                $games = $this->getUser()->getGames();
                foreach ($games as $game) {
                   $game->setFavorite(true);
                }
            } else {
                $games = $repo->findAll();
                if ($this->getUser() != null) {
                    $favs = $this->getUser()->getGames()->toArray();
                    $arrayFavs = [];
                    foreach ($favs as $fav) {
                        array_push($arrayFavs, $fav->getId());
                    }

                    foreach ($games as $game) {
                        if (in_array($game->getId(), $arrayFavs)) {
                            $game->setFavorite(true);
                        } else {
                            $game->setFavorite(false);
                        }
                    }
                }
            }
        }

        $genres = $repogenres->findAll();

        if (isset($_GET['genres'])) {
            return $this->render('games/index.html.twig', [
                'controller_name' => 'GamesController',
                'games' => $games_genres,
                'genres' => $genres,

            ]);
        } else {
            return $this->render('games/index.html.twig', [
                'controller_name' => 'GamesController',
                'games' => $games,
                'genres' => $genres,
            ]);
        }
    }

    #[Route('/games/fav/{id}', name: 'games_fav')]
    public function fav(GamesRepository $repo, Request $request, EntityManagerInterface $manager, $id): Response
    {
        $game = $repo->find($id);
        $usersInGame = $game->getUsers()->toArray();
        $idsusers = [];
        foreach ($usersInGame as $userInGame) {
            array_push($idsusers, $userInGame->getEmail());
        }

        $useremail = $this->getUser()->getEmail();

        $user = $this->getUser();

        $form = $this->createForm(GamesType::class, $game);

        $form->handleRequest($request);

        if (empty($idsusers)) {
            $game->addUser($user);
            $game->setFavorite(true);
        } else {
            foreach ($idsusers as $idusers) {
                if ($idusers === $useremail) {
                    $game->removeUser($user);
                    $game->setFavorite(false);
                } else {
                    $game->addUser($user);
                    $game->setFavorite(true);
                }
            }
        }

        $manager->persist($game);

        $manager->flush();

        return $this->redirectToRoute('games');
    }

    // ***************************** Partie gérée par easyadmin *****************************

    /*     #[Route('/games/new', name: 'games_create')]
    #[Route('/games/{id}/edit', name: 'games_edit')]
    public function edit(Games $game = null, Request $request, EntityManagerInterface $manager, SluggerInterface $slugger): Response
    {

        if(!$game) {
            $game = new Games();
        }

        $form = $this->createForm(GamesType::class, $game);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            if(!$game->getId()) {

                $gamesFile = $form->get('Image')->getData();

            // so the file must be processed only when a file is uploaded
            if ($gamesFile) {
                $originalFilename = pathinfo($gamesFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$gamesFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $gamesFile->move(
                        $this->getParameter('images_games_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $game->setImage($newFilename);
                }
            }

            $manager->persist($game);
            $manager->flush();

            return $this->redirectToRoute('games_show', ['id' => $game->getId()]);
        }

        return $this->render('games/edit.html.twig', [
            'formGames' => $form->createView(),
            'editMode' => $game->getId() !== null
        ]);
    }
 */
    #[Route('/games/{id}', name: 'games_show')]
    public function show(GamesRepository $repo, Notes $note = null, NotesRepository $reponotes, Request $request, EntityManagerInterface $manager, $id,): Response
    {
        $game = $repo->find($id);

        $games = $repo->findAll();

        $user = $this->getUser();

        foreach ($games as $jeu) {

            $usersInGame = $jeu->getUsers()->toArray();
            $idsusers = [];
            foreach ($usersInGame as $userInGame) {
                array_push($idsusers, $userInGame->getEmail());
            }

            if ($this->getUser() != null) {
                $useremail = $this->getUser()->getEmail();
                foreach ($idsusers as $idusers) {
                    if ($idusers === $useremail) {
                        $jeu->setFavorite(true);
                    } else {
                        $jeu->setFavorite(false);
                    }
                }
            }

            if (!$note) {
                $note = new Notes();
            }

            $form = $this->createForm(NotesType::class, $note);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                if (!$note->getId()) {
                    $note->setDate(new \DateTime());
                    $note->setEtat('1');
                    $note->setGame($game);
                    $note->setUser($user);
                }
                $manager->persist($note);
                $manager->flush();
            }

            $avg = $reponotes->avgNote($id);

            return $this->render('games/show.html.twig', [
                'game' => $game,
                'formNote' => $form->createView(),
                'avg' => $avg
            ]);
        }
    }
}
