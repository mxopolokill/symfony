<?php

namespace App\Entity;

use App\Repository\GenresRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use symfony\component\HttpFoundation\File\File;


/**
 * @ORM\Entity(repositoryClass=GenresRepository::class)
 * @Vich\Uploadable
 */
class Genres
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Image;

    /**
     * @ORM\ManyToMany(targetEntity=Games::class, mappedBy="genres")
     */
    private $games;

    /**
     * @Vich\UploadableField(mapping="images_genres", fileNameProperty="Image")
     * @var File
     */
    public $Uploadfile;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->Image;
    }

    /**
     *@param string|null $Image
     *@return $this 
    */
    public function setImage(?string $Image): self
    {
        $this->Image = $Image;

        return $this;
    }

    public function setImageFile(?File $Image): self
    {
        $this->ImageFile = $Image;
     if($Image) $this->updatedAt = new \DateTime();
     return $this;
    }


    /**
     * @return Collection|Games[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Games $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->addGenre($this);
        }

        return $this;
    }

    public function removeGame(Games $game): self
    {
        if ($this->games->removeElement($game)) {
            $game->removeGenre($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->Title;
    }
}
