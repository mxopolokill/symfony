<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Games;
use App\Entity\Genres;
use App\Entity\Users;
use App\Entity\Notes;
use Faker;

class GamesFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

         // Créer des catégories
        for($i = 1; $i <= 5; $i++) {
            $types = new Genres();
            $types->setTitle($faker->sentence())
                  ->setDescription($faker->paragraph())
                  ->setImage($faker->imageUrl);
        
            $manager->persist($types);

            // Créer des jeux pour chaque catégorie
            for($j = 1; $j <= mt_rand(1, 5); $j++) {
                $game = new Games();
    
                $game->setTitle($faker->text)
                     ->setDescription($faker->text)
                     ->setImage($faker->mimetype . '.jpg')
                     ->setDate($faker->dateTimeBetween('-6 months'))
                     ->addGenre($types);
    
                $manager->persist($game); 

                // Créer des utilisateurs pour chaque jeu
                for($k = 1; $k <= mt_rand(1, 5); $k++) {
                    $user = new Users();
                    $array = [0, 1];
        
                    $user->setPseudonyme($faker->text)
                         ->setEmail($faker->email)
                         ->setPassword($faker->password)
                         ->setRoles($array)
                         ->addGame($game);
        
                    $manager->persist($user);


               // On donne des opinions aux jeux
                for($l = 1; $l <= mt_rand(1, 3); $l++) {

                    $now = new \DateTime();
                    $days = $now->diff($game->getDate())->days;

                    $notes = new Notes();
                    $notes->setNote($faker->numberBetween(1, 5))
                             ->setDescription($faker->sentence())
                             ->setDate($faker->dateTimeBetween('-' . $days . 'days'))
                             ->setEtat($faker->boolean($chanceOfGettingTrue = 50))
                             ->setGame($game)
                             ->setUser($user);

                    $manager->persist($notes);
                } 
            } 
        }

        $manager->flush();
    }
}
}
