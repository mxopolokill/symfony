<?php

namespace App\Repository;

use App\Entity\Pseudonyme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pseudonyme|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pseudonyme|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pseudonyme[]    findAll()
 * @method Pseudonyme[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PseudonymeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pseudonyme::class);
    }

    // /**
    //  * @return Pseudonyme[] Returns an array of Pseudonyme objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pseudonyme
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
