<?php

namespace Container7y8P1So;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder179ee = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer3110f = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties081d2 = [
        
    ];

    public function getConnection()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getConnection', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getMetadataFactory', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getExpressionBuilder', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'beginTransaction', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getCache', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getCache();
    }

    public function transactional($func)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'transactional', array('func' => $func), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'wrapInTransaction', array('func' => $func), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'commit', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->commit();
    }

    public function rollback()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'rollback', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getClassMetadata', array('className' => $className), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'createQuery', array('dql' => $dql), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'createNamedQuery', array('name' => $name), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'createQueryBuilder', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'flush', array('entity' => $entity), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'clear', array('entityName' => $entityName), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->clear($entityName);
    }

    public function close()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'close', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->close();
    }

    public function persist($entity)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'persist', array('entity' => $entity), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'remove', array('entity' => $entity), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'refresh', array('entity' => $entity), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'detach', array('entity' => $entity), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'merge', array('entity' => $entity), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getRepository', array('entityName' => $entityName), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'contains', array('entity' => $entity), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getEventManager', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getConfiguration', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'isOpen', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getUnitOfWork', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getProxyFactory', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'initializeObject', array('obj' => $obj), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'getFilters', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'isFiltersStateClean', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'hasFilters', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return $this->valueHolder179ee->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer3110f = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder179ee) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder179ee = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder179ee->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, '__get', ['name' => $name], $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        if (isset(self::$publicProperties081d2[$name])) {
            return $this->valueHolder179ee->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder179ee;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder179ee;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder179ee;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder179ee;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, '__isset', array('name' => $name), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder179ee;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder179ee;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, '__unset', array('name' => $name), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder179ee;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder179ee;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, '__clone', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        $this->valueHolder179ee = clone $this->valueHolder179ee;
    }

    public function __sleep()
    {
        $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, '__sleep', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;

        return array('valueHolder179ee');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer3110f = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer3110f;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer3110f && ($this->initializer3110f->__invoke($valueHolder179ee, $this, 'initializeProxy', array(), $this->initializer3110f) || 1) && $this->valueHolder179ee = $valueHolder179ee;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder179ee;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder179ee;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
