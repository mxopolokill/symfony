<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* games/_content.html.twig */
class __TwigTemplate_933910c69f191078f75dc8a282faf7235d4be9cb1a014c69bbd73d231c105434 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "games/_content.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "games/_content.html.twig"));

        // line 1
        echo " 
    ";
        // line 2
        $context["queryParams"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 2, $this->source); })()), "request", [], "any", false, false, false, 2), "query", [], "any", false, false, false, 2), "all", [], "any", false, false, false, 2);
        // line 3
        echo "    ";
        $context["queryString"] = twig_urlencode_filter(twig_array_filter($this->env, (isset($context["queryParams"]) || array_key_exists("queryParams", $context) ? $context["queryParams"] : (function () { throw new RuntimeError('Variable "queryParams" does not exist.', 3, $this->source); })()), function ($__v__, $__k__) use ($context, $macros) { $context["v"] = $__v__; $context["k"] = $__k__; return (((isset($context["k"]) || array_key_exists("k", $context) ? $context["k"] : (function () { throw new RuntimeError('Variable "k" does not exist.', 3, $this->source); })()) != "page") && ((isset($context["k"]) || array_key_exists("k", $context) ? $context["k"] : (function () { throw new RuntimeError('Variable "k" does not exist.', 3, $this->source); })()) != "ajax")); }));
        // line 4
        echo "
<ul class=\"pagination\">
        <li class=\"";
        // line 6
        echo ((((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 6, $this->source); })()) == 1)) ? ("disabled") : (""));
        echo "\">
            <a href=\"?page=1&";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["queryString"]) || array_key_exists("queryString", $context) ? $context["queryString"] : (function () { throw new RuntimeError('Variable "queryString" does not exist.', 7, $this->source); })()), "html", null, true);
        echo "\">
                <i class=\"fas fa-step-backward\"></i>
            </a>
        </li>    
        <li class=\"";
        // line 11
        echo ((((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 11, $this->source); })()) == 1)) ? ("disabled") : (""));
        echo "\">
            <a href=\"";
        // line 12
        ((((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 12, $this->source); })()) > 1)) ? (print (twig_escape_filter($this->env, ("?page=" . ((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 12, $this->source); })()) - 1)), "html", null, true))) : (print ("")));
        echo "&";
        echo twig_escape_filter($this->env, (isset($context["queryString"]) || array_key_exists("queryString", $context) ? $context["queryString"] : (function () { throw new RuntimeError('Variable "queryString" does not exist.', 12, $this->source); })()), "html", null, true);
        echo "\">
                <i class=\"fas fa-angle-left\"></i>
            </a>
        </li>
        ";
        // line 16
        $context["pages"] = twig_round(((isset($context["total"]) || array_key_exists("total", $context) ? $context["total"] : (function () { throw new RuntimeError('Variable "total" does not exist.', 16, $this->source); })()) / (isset($context["limit"]) || array_key_exists("limit", $context) ? $context["limit"] : (function () { throw new RuntimeError('Variable "limit" does not exist.', 16, $this->source); })())), 0, "ceil");
        // line 17
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["pages"]) || array_key_exists("pages", $context) ? $context["pages"] : (function () { throw new RuntimeError('Variable "pages" does not exist.', 17, $this->source); })())));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 18
            echo "            <li class=\"";
            echo ((((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 18, $this->source); })()) == $context["item"])) ? ("active") : (""));
            echo "\">
                <a href=\"?page=";
            // line 19
            echo twig_escape_filter($this->env, $context["item"], "html", null, true);
            echo "&";
            echo twig_escape_filter($this->env, (isset($context["queryString"]) || array_key_exists("queryString", $context) ? $context["queryString"] : (function () { throw new RuntimeError('Variable "queryString" does not exist.', 19, $this->source); })()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["item"], "html", null, true);
            echo "</a>
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "        <li class=\"";
        echo ((((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 22, $this->source); })()) == (isset($context["pages"]) || array_key_exists("pages", $context) ? $context["pages"] : (function () { throw new RuntimeError('Variable "pages" does not exist.', 22, $this->source); })()))) ? ("disabled") : (""));
        echo "\">
            <a href=\"";
        // line 23
        ((((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 23, $this->source); })()) < (isset($context["pages"]) || array_key_exists("pages", $context) ? $context["pages"] : (function () { throw new RuntimeError('Variable "pages" does not exist.', 23, $this->source); })()))) ? (print (twig_escape_filter($this->env, ("?page=" . ((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 23, $this->source); })()) + 1)), "html", null, true))) : (print ("")));
        echo "&";
        echo twig_escape_filter($this->env, (isset($context["queryString"]) || array_key_exists("queryString", $context) ? $context["queryString"] : (function () { throw new RuntimeError('Variable "queryString" does not exist.', 23, $this->source); })()), "html", null, true);
        echo "\">
                <i class=\"fas fa-angle-right\"></i>
            </a>
        </li>
        <li class=\"";
        // line 27
        echo ((((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 27, $this->source); })()) == (isset($context["pages"]) || array_key_exists("pages", $context) ? $context["pages"] : (function () { throw new RuntimeError('Variable "pages" does not exist.', 27, $this->source); })()))) ? ("disabled") : (""));
        echo "\">
            <a href=\"?page=";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["pages"]) || array_key_exists("pages", $context) ? $context["pages"] : (function () { throw new RuntimeError('Variable "pages" does not exist.', 28, $this->source); })()), "html", null, true);
        echo "&";
        echo twig_escape_filter($this->env, (isset($context["queryString"]) || array_key_exists("queryString", $context) ? $context["queryString"] : (function () { throw new RuntimeError('Variable "queryString" does not exist.', 28, $this->source); })()), "html", null, true);
        echo "\">
                <i class=\"fas fa-step-forward\"></i>
            </a>
        </li>

    </ul>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "games/_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 28,  118 => 27,  109 => 23,  104 => 22,  91 => 19,  86 => 18,  81 => 17,  79 => 16,  70 => 12,  66 => 11,  59 => 7,  55 => 6,  51 => 4,  48 => 3,  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source(" 
    {% set queryParams = app.request.query.all %}
    {% set queryString = queryParams|filter((v, k) => k != \"page\" and k != \"ajax\")|url_encode %}

<ul class=\"pagination\">
        <li class=\"{{ (page == 1) ? 'disabled' : '' }}\">
            <a href=\"?page=1&{{queryString}}\">
                <i class=\"fas fa-step-backward\"></i>
            </a>
        </li>    
        <li class=\"{{ (page == 1) ? 'disabled' : '' }}\">
            <a href=\"{{ (page > 1) ? '?page=' ~ (page - 1) : '' }}&{{queryString}}\">
                <i class=\"fas fa-angle-left\"></i>
            </a>
        </li>
        {% set pages = (total / limit)|round(0, 'ceil') %}
        {% for item in 1..pages %}
            <li class=\"{{ (page == item) ? 'active' : '' }}\">
                <a href=\"?page={{ item }}&{{queryString}}\">{{ item }}</a>
            </li>
        {% endfor %}
        <li class=\"{{ (page == pages) ? 'disabled' : '' }}\">
            <a href=\"{{ (page < pages) ? '?page=' ~ (page + 1) : '' }}&{{queryString}}\">
                <i class=\"fas fa-angle-right\"></i>
            </a>
        </li>
        <li class=\"{{ (page == pages) ? 'disabled' : '' }}\">
            <a href=\"?page={{ pages }}&{{queryString}}\">
                <i class=\"fas fa-step-forward\"></i>
            </a>
        </li>

    </ul>", "games/_content.html.twig", "C:\\wamp64\\www\\symfony\\templates\\games\\_content.html.twig");
    }
}
