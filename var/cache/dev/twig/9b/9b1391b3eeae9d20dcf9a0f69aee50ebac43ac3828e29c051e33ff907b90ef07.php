<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* games/show.html.twig */
class __TwigTemplate_caa1d97f7b06578ee94ad22b13fb5902d5099f5c2fbbe304284d645a0a75ce3b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "games/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "games/show.html.twig"));

        // line 3
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["formNote"]) || array_key_exists("formNote", $context) ? $context["formNote"] : (function () { throw new RuntimeError('Variable "formNote" does not exist.', 3, $this->source); })()), [0 => "bootstrap_5_layout.html.twig"], true);
        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "games/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["game"]) || array_key_exists("game", $context) ? $context["game"] : (function () { throw new RuntimeError('Variable "game" does not exist.', 6, $this->source); })()), "title", [], "any", false, false, false, 6), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo "        <div class=\"title d-flex\">
            <h2>";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["game"]) || array_key_exists("game", $context) ? $context["game"] : (function () { throw new RuntimeError('Variable "game" does not exist.', 12, $this->source); })()), "title", [], "any", false, false, false, 12), "html", null, true);
        echo "</h2>
            ";
        // line 13
        if ((twig_get_attribute($this->env, $this->source, (isset($context["game"]) || array_key_exists("game", $context) ? $context["game"] : (function () { throw new RuntimeError('Variable "game" does not exist.', 13, $this->source); })()), "favorite", [], "any", false, false, false, 13) == true)) {
            // line 14
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("games_fav", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["game"]) || array_key_exists("game", $context) ? $context["game"] : (function () { throw new RuntimeError('Variable "game" does not exist.', 14, $this->source); })()), "id", [], "any", false, false, false, 14)]), "html", null, true);
            echo "\" <i class=\"fas fa-heart fa-3x\"></i></a>
                    ";
        } else {
            // line 16
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("games_fav", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["game"]) || array_key_exists("game", $context) ? $context["game"] : (function () { throw new RuntimeError('Variable "game" does not exist.', 16, $this->source); })()), "id", [], "any", false, false, false, 16)]), "html", null, true);
            echo "\" <i class=\"far fa-heart fa-3x\"></i></a>
                    ";
        }
        // line 18
        echo "        </div>  
        
        <div class=\"metadata\">
            Crée le ";
        // line 21
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["game"]) || array_key_exists("game", $context) ? $context["game"] : (function () { throw new RuntimeError('Variable "game" does not exist.', 21, $this->source); })()), "date", [], "any", false, false, false, 21), "d/m/Y"), "html", null, true);
        echo " à ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["game"]) || array_key_exists("game", $context) ? $context["game"] : (function () { throw new RuntimeError('Variable "game" does not exist.', 21, $this->source); })()), "date", [], "any", false, false, false, 21), "H:i"), "html", null, true);
        echo "
        </div>
        ";
        // line 23
        if ((twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["avg"]) || array_key_exists("avg", $context) ? $context["avg"] : (function () { throw new RuntimeError('Variable "avg" does not exist.', 23, $this->source); })()), "content", [], "any", false, false, false, 23), 1) != 0)) {
            // line 24
            echo "        <div class=\"Stars\" style=\"--rating: ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["avg"]) || array_key_exists("avg", $context) ? $context["avg"] : (function () { throw new RuntimeError('Variable "avg" does not exist.', 24, $this->source); })()), "content", [], "any", false, false, false, 24), 1), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["avg"]) || array_key_exists("avg", $context) ? $context["avg"] : (function () { throw new RuntimeError('Variable "avg" does not exist.', 24, $this->source); })()), "content", [], "any", false, false, false, 24), 1), "html", null, true);
            echo "/5</div>
        ";
        } else {
            // line 26
            echo "        <div class=\"Stars\" style=\"--rating: ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["avg"]) || array_key_exists("avg", $context) ? $context["avg"] : (function () { throw new RuntimeError('Variable "avg" does not exist.', 26, $this->source); })()), "content", [], "any", false, false, false, 26), 1), "html", null, true);
            echo "\">Non noté</div>
        ";
        }
        // line 28
        echo "        <div class=\"content\">
            <img src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images_games/" . twig_get_attribute($this->env, $this->source, (isset($context["game"]) || array_key_exists("game", $context) ? $context["game"] : (function () { throw new RuntimeError('Variable "game" does not exist.', 29, $this->source); })()), "image", [], "any", false, false, false, 29))), "html", null, true);
        echo "\" alt=\"\">
            <p>";
        // line 30
        echo twig_get_attribute($this->env, $this->source, (isset($context["game"]) || array_key_exists("game", $context) ? $context["game"] : (function () { throw new RuntimeError('Variable "game" does not exist.', 30, $this->source); })()), "description", [], "any", false, false, false, 30);
        echo "</p>
        </div>
    </div>

    
    <div class=\"container\">
        <div class=\"row d-flex\">
            <div class=\"col-md-8\">
                <div class=\"headings d-flex justify-content-between align-items-center mb-3\">
                    <p>Comments<p>
                </div>
                ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["game"]) || array_key_exists("game", $context) ? $context["game"] : (function () { throw new RuntimeError('Variable "game" does not exist.', 41, $this->source); })()), "notes", [], "any", false, false, false, 41));
        foreach ($context['_seq'] as $context["_key"] => $context["note"]) {
            // line 42
            echo "                ";
            if ((twig_get_attribute($this->env, $this->source, $context["note"], "etat", [], "any", false, false, false, 42) == 1)) {
                // line 43
                echo "                <div class=\"card p-3 m-3\">
                    <div class=\"d-flex justify-content-between align-items-center\">
                        <div class=\"user d-flex flex-row align-items-center\"> 
                            <span>
                                <strong><small class=\"font-weight-bold text-primary\">";
                // line 47
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["note"], "user", [], "any", false, false, false, 47), "pseudonyme", [], "any", false, false, false, 47), "html", null, true);
                echo "</small></strong>
                                <small class=\"font-weight-bold\">";
                // line 48
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["note"], "description", [], "any", false, false, false, 48), "html", null, true);
                echo " </small>
                            </span> 
                        </div> 
                        <small>";
                // line 51
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["note"], "date", [], "any", false, false, false, 51), "d/m/Y"), "html", null, true);
                echo " à ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["note"], "date", [], "any", false, false, false, 51), "H:i"), "html", null, true);
                echo "</small>
                    </div>
                    <div class=\"Stars_min\" style=\"--rating: ";
                // line 53
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["note"], "note", [], "any", false, false, false, 53), "html", null, true);
                echo "\"></div>
                </div>
                ";
            }
            // line 56
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['note'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "            </div>
        </div>

        ";
        // line 60
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 60), "pseudonyme", [], "any", true, true, false, 60)) {
            // line 61
            echo "            ";
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formNote"]) || array_key_exists("formNote", $context) ? $context["formNote"] : (function () { throw new RuntimeError('Variable "formNote" does not exist.', 61, $this->source); })()), 'form_start');
            echo "

            ";
            // line 63
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formNote"]) || array_key_exists("formNote", $context) ? $context["formNote"] : (function () { throw new RuntimeError('Variable "formNote" does not exist.', 63, $this->source); })()), "description", [], "any", false, false, false, 63), 'label');
            echo "
            ";
            // line 64
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formNote"]) || array_key_exists("formNote", $context) ? $context["formNote"] : (function () { throw new RuntimeError('Variable "formNote" does not exist.', 64, $this->source); })()), "description", [], "any", false, false, false, 64), 'widget');
            echo "

            ";
            // line 66
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formNote"]) || array_key_exists("formNote", $context) ? $context["formNote"] : (function () { throw new RuntimeError('Variable "formNote" does not exist.', 66, $this->source); })()), "note", [], "any", false, false, false, 66), 'label');
            echo "
            ";
            // line 67
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formNote"]) || array_key_exists("formNote", $context) ? $context["formNote"] : (function () { throw new RuntimeError('Variable "formNote" does not exist.', 67, $this->source); })()), "note", [], "any", false, false, false, 67), 'widget');
            echo "    
            <button type=\"submit\" class=\"btn btn-primary\">Envoyer</button>

            ";
            // line 70
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formNote"]) || array_key_exists("formNote", $context) ? $context["formNote"] : (function () { throw new RuntimeError('Variable "formNote" does not exist.', 70, $this->source); })()), 'form_end');
            echo "
        ";
        } else {
            // line 72
            echo "        <p>Veuillez vous connecter pour ajouter un avis.</p>
        ";
        }
        // line 74
        echo "    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "games/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 74,  240 => 72,  235 => 70,  229 => 67,  225 => 66,  220 => 64,  216 => 63,  210 => 61,  208 => 60,  203 => 57,  197 => 56,  191 => 53,  184 => 51,  178 => 48,  174 => 47,  168 => 43,  165 => 42,  161 => 41,  147 => 30,  143 => 29,  140 => 28,  134 => 26,  126 => 24,  124 => 23,  117 => 21,  112 => 18,  106 => 16,  100 => 14,  98 => 13,  94 => 12,  91 => 11,  81 => 10,  62 => 6,  51 => 1,  49 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %} 

{% form_theme formNote 'bootstrap_5_layout.html.twig' %}


{% block title %}{{ game.title }}{% endblock %}



{% block body %}
        <div class=\"title d-flex\">
            <h2>{{ game.title }}</h2>
            {% if game.favorite == true %}
                    <a href=\"{{ path('games_fav', {'id' : game.id}) }}\" <i class=\"fas fa-heart fa-3x\"></i></a>
                    {% else %}
                    <a href=\"{{ path('games_fav', {'id' : game.id}) }}\" <i class=\"far fa-heart fa-3x\"></i></a>
                    {% endif %}
        </div>  
        
        <div class=\"metadata\">
            Crée le {{ game.date | date('d/m/Y') }} à {{ game.date | date('H:i')}}
        </div>
        {% if avg.content|number_format(1) != 0 %}
        <div class=\"Stars\" style=\"--rating: {{ avg.content|number_format(1) }}\">{{ avg.content|number_format(1) }}/5</div>
        {% else %}
        <div class=\"Stars\" style=\"--rating: {{ avg.content|number_format(1) }}\">Non noté</div>
        {% endif %}
        <div class=\"content\">
            <img src=\"{{ asset('uploads/images_games/' ~ game.image) }}\" alt=\"\">
            <p>{{ game.description | raw }}</p>
        </div>
    </div>

    
    <div class=\"container\">
        <div class=\"row d-flex\">
            <div class=\"col-md-8\">
                <div class=\"headings d-flex justify-content-between align-items-center mb-3\">
                    <p>Comments<p>
                </div>
                {% for note in game.notes %}
                {% if note.etat == 1 %}
                <div class=\"card p-3 m-3\">
                    <div class=\"d-flex justify-content-between align-items-center\">
                        <div class=\"user d-flex flex-row align-items-center\"> 
                            <span>
                                <strong><small class=\"font-weight-bold text-primary\">{{ note.user.pseudonyme }}</small></strong>
                                <small class=\"font-weight-bold\">{{ note.description }} </small>
                            </span> 
                        </div> 
                        <small>{{ note.date | date('d/m/Y') }} à {{ note.date | date('H:i')}}</small>
                    </div>
                    <div class=\"Stars_min\" style=\"--rating: {{ note.note }}\"></div>
                </div>
                {% endif %}
                {% endfor %}
            </div>
        </div>

        {% if app.user.pseudonyme is defined %}
            {{ form_start(formNote) }}

            {{ form_label(formNote.description) }}
            {{ form_widget(formNote.description) }}

            {{ form_label(formNote.note) }}
            {{ form_widget(formNote.note) }}    
            <button type=\"submit\" class=\"btn btn-primary\">Envoyer</button>

            {{ form_end(formNote) }}
        {% else %}
        <p>Veuillez vous connecter pour ajouter un avis.</p>
        {% endif %}
    </div>
</div>
{% endblock %}

", "games/show.html.twig", "C:\\wamp64\\www\\symfony\\templates\\games\\show.html.twig");
    }
}
