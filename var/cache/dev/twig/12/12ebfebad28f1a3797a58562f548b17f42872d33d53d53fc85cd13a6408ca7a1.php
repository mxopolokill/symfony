<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* games/index.html.twig */
class __TwigTemplate_34c661be5c69649f21e7917d922cab84663b1e7289c864a643b20867cc0cb36d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "games/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "games/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "games/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 4, $this->source); })()), "request", [], "any", false, false, false, 4), "requestUri", [], "any", false, false, false, 4) == "/games?fav")) {
            // line 5
            echo "        Favoris
    ";
        } else {
            // line 7
            echo "        Jeux
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "
</div>

<section id=\"games\">
    ";
        // line 16
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 16, $this->source); })()), "request", [], "any", false, false, false, 16), "requestUri", [], "any", false, false, false, 16) == "/games?fav")) {
            // line 17
            echo "    <h2>Favoris</h2>
    ";
        } else {
            // line 19
            echo "    <h2>Jeux</h2>
    ";
        }
        // line 21
        echo "</section>

<div class=\"container py-3\">
    <div class=\"row align-items-center\">
        <div class=\"col-6\">
            <form class=\"flex-row\" id=\"filters\" method=\"get\" action=\"\">
                <label class=\"mx-2\" for=\"genres[]\">Genres</label>
                    <select name=\"genres[]\" id=\"genres[]\" multiple>
                    ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["genres"]) || array_key_exists("genres", $context) ? $context["genres"] : (function () { throw new RuntimeError('Variable "genres" does not exist.', 29, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["genre"]) {
            // line 30
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["genre"], "id", [], "any", false, false, false, 30), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["genre"], "title", [], "any", false, false, false, 30), "html", null, true);
            echo "</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['genre'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "                    </select>
                <button type=\"submit\" class=\"btn btn-yellow mx-3 w-25\">Filtrer</button>
            </form>
        </div>
        <div class=\"col-6\">
            <form class=\"flex-row\" id=\"order\" method=\"get\" action=\"\">
                <label class=\"mx-2\" for=\"order\">Ordre</label>
                    <select name=\"order\" id=\"order\">
                        <option value=\"asc\">A-Z</option>
                        <option value=\"desc\">Z-A</option>
                    </select>
                <button type=\"submit\" class=\"btn btn-yellow mx-3 w-25\">Valider</button>
            </form>
        </div>
    </div>

    <div class=\"row results-list\">
        ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["games"]) || array_key_exists("games", $context) ? $context["games"] : (function () { throw new RuntimeError('Variable "games" does not exist.', 49, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["game"]) {
            // line 50
            echo "        <div class=\"col cards-container d-flex justify-content-evenly\">
            <div class=\"card-one\">
                <header>
                    <div class=\"image\">
                        <img src=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images_games/" . twig_get_attribute($this->env, $this->source, $context["game"], "image", [], "any", false, false, false, 54))), "html", null, true);
            echo "\" alt=\"photo jeux\" />
                    </div>
                </header>
                <div class=\"infos\">
                    <h3><strong>";
            // line 58
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["game"], "title", [], "any", false, false, false, 58), "html", null, true);
            echo "</strong></h3>
                </div>
                <div class=\"desc\">
                    <p>";
            // line 61
            echo twig_get_attribute($this->env, $this->source, $context["game"], "description", [], "any", false, false, false, 61);
            echo "</p>
                </div>
                <div class=\"promotion\">
                    ";
            // line 64
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["game"], "genres", [], "any", false, false, false, 64));
            foreach ($context['_seq'] as $context["_key"] => $context["genre"]) {
                // line 65
                echo "                    <p class=\"promotion-bordure\">";
                echo twig_escape_filter($this->env, $context["genre"], "html", null, true);
                echo "</p>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['genre'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "                </div>
                <div class=\"skills row\">
                    <p> Crée le ";
            // line 69
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["game"], "date", [], "any", false, false, false, 69), "d/m/Y"), "html", null, true);
            echo " à ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["game"], "date", [], "any", false, false, false, 69), "H:i"), "html", null, true);
            echo "</p>
                </div>

                <footer>
                    <a href=\"";
            // line 73
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["game"], "url", [], "any", false, false, false, 73), "html", null, true);
            echo "\" target=\"_blank\"><i class=\"fas fa-gamepad mr-1\"></i>Jouer</a>
                    <a href=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("games_show", ["id" => twig_get_attribute($this->env, $this->source, $context["game"], "id", [], "any", false, false, false, 74)]), "html", null, true);
            echo "\"><i class=\"fas fa-book-reader mr-1\"></i>Avis</a>
                        ";
            // line 75
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 75), "pseudonyme", [], "any", true, true, false, 75)) {
                // line 76
                echo "                            ";
                if ((twig_get_attribute($this->env, $this->source, $context["game"], "favorite", [], "any", false, false, false, 76) == true)) {
                    // line 77
                    echo "                                <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("games_fav", ["id" => twig_get_attribute($this->env, $this->source, $context["game"], "id", [], "any", false, false, false, 77)]), "html", null, true);
                    echo "\" <i class=\"fas fa-heart coeur\"></i></a>
                            ";
                } else {
                    // line 79
                    echo "                                <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("games_fav", ["id" => twig_get_attribute($this->env, $this->source, $context["game"], "id", [], "any", false, false, false, 79)]), "html", null, true);
                    echo "\" <i class=\"far fa-heart\"></i></a>
                            ";
                }
                // line 81
                echo "                        ";
            } else {
                // line 82
                echo "                        ";
            }
            // line 83
            echo "                </footer>
            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['game'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "    </div>
</div>
</section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "games/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 87,  244 => 83,  241 => 82,  238 => 81,  232 => 79,  226 => 77,  223 => 76,  221 => 75,  217 => 74,  213 => 73,  204 => 69,  200 => 67,  191 => 65,  187 => 64,  181 => 61,  175 => 58,  168 => 54,  162 => 50,  158 => 49,  139 => 32,  128 => 30,  124 => 29,  114 => 21,  110 => 19,  106 => 17,  104 => 16,  98 => 12,  88 => 11,  76 => 7,  72 => 5,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
    {% if (app.request.requestUri == '/games?fav') %}
        Favoris
    {% else %}
        Jeux
    {% endif %}
{% endblock %}

{% block body %}

</div>

<section id=\"games\">
    {% if (app.request.requestUri == '/games?fav') %}
    <h2>Favoris</h2>
    {% else %}
    <h2>Jeux</h2>
    {% endif %}
</section>

<div class=\"container py-3\">
    <div class=\"row align-items-center\">
        <div class=\"col-6\">
            <form class=\"flex-row\" id=\"filters\" method=\"get\" action=\"\">
                <label class=\"mx-2\" for=\"genres[]\">Genres</label>
                    <select name=\"genres[]\" id=\"genres[]\" multiple>
                    {% for genre in genres %}
                        <option value=\"{{ genre.id }}\">{{ genre.title }}</option>
                    {% endfor %}
                    </select>
                <button type=\"submit\" class=\"btn btn-yellow mx-3 w-25\">Filtrer</button>
            </form>
        </div>
        <div class=\"col-6\">
            <form class=\"flex-row\" id=\"order\" method=\"get\" action=\"\">
                <label class=\"mx-2\" for=\"order\">Ordre</label>
                    <select name=\"order\" id=\"order\">
                        <option value=\"asc\">A-Z</option>
                        <option value=\"desc\">Z-A</option>
                    </select>
                <button type=\"submit\" class=\"btn btn-yellow mx-3 w-25\">Valider</button>
            </form>
        </div>
    </div>

    <div class=\"row results-list\">
        {% for game in games %}
        <div class=\"col cards-container d-flex justify-content-evenly\">
            <div class=\"card-one\">
                <header>
                    <div class=\"image\">
                        <img src=\"{{ asset('uploads/images_games/' ~ game.image) }}\" alt=\"photo jeux\" />
                    </div>
                </header>
                <div class=\"infos\">
                    <h3><strong>{{ game.title }}</strong></h3>
                </div>
                <div class=\"desc\">
                    <p>{{ game.description | raw }}</p>
                </div>
                <div class=\"promotion\">
                    {% for genre in game.genres %}
                    <p class=\"promotion-bordure\">{{ genre }}</p>
                    {% endfor %}
                </div>
                <div class=\"skills row\">
                    <p> Crée le {{ game.date | date('d/m/Y') }} à {{ game.date | date('H:i')}}</p>
                </div>

                <footer>
                    <a href=\"{{ game.url }}\" target=\"_blank\"><i class=\"fas fa-gamepad mr-1\"></i>Jouer</a>
                    <a href=\"{{ path('games_show', {'id' : game.id}) }}\"><i class=\"fas fa-book-reader mr-1\"></i>Avis</a>
                        {% if app.user.pseudonyme is defined %}
                            {% if game.favorite == true %}
                                <a href=\"{{ path('games_fav', {'id' : game.id}) }}\" <i class=\"fas fa-heart coeur\"></i></a>
                            {% else %}
                                <a href=\"{{ path('games_fav', {'id' : game.id}) }}\" <i class=\"far fa-heart\"></i></a>
                            {% endif %}
                        {% else %}
                        {% endif %}
                </footer>
            </div>
        </div>
        {% endfor %}
    </div>
</div>
</section>

{% endblock %}
", "games/index.html.twig", "C:\\wamp64\\www\\Nouveau dossier\\symfony\\templates\\games\\index.html.twig");
    }
}
